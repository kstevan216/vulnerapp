# Vulnerapp

-- A Vulnerable Sample Spring Boot Application

This application uses a relatively modern stack but is still vulnerable to a set of attacks.
Featuring:

- [XSS](https://portswigger.net/web-security/cross-site-scripting)
- [SQLi](https://portswigger.net/web-security/sql-injection)
- [CSRF](https://portswigger.net/web-security/csrf)
- [SSRF](https://portswigger.net/web-security/ssrf)
- Fake Logins
- Info Exposure
- Plain Passwords
- ...

```console
./gradlew bootRun
```

# Diskussion und Selbstevaluation

1. Verwendung von korrekten REST-Verben (Get, Post, Put, Delete): Durch die korrekte Verwendung dieser Verben
   ermöglichen wir eine klare und konsistente
   Kommunikation zwischen Client und Server. Dies verbessert die Sicherheit, da die beabsichtigten Aktionen nur
   durchgeführt werden können, wenn der entsprechende Verben-Typ verwendet wird.
2. Implementierung einer
   Authentifizierungslösung (z.B. BasicAuth): Dies ermöglicht es, den Zugriff auf geschützte Ressourcen auf
   authentifizierte Benutzer zu
   beschränken. Durch die Verwendung von Benutzernamen und Passwörtern wird sichergestellt, dass nur autorisierte
   Benutzer
   auf sensible Informationen zugreifen können. Diese Implementierung ist sinnvoll, um die Vertraulichkeit und den
   Schutz
   von Benutzerdaten zu gewährleisten.
3. Enforce RBAC (z.B. User- und Admin-Services unterscheiden): Dadurch können wir bestimmte Funktionen oder
   Ressourcen nur für bestimmte Benutzerrollen zugänglich machen. Dies erhöht die Sicherheit, da nur autorisierte
   Benutzer
   die erforderlichen Berechtigungen haben, um bestimmte Aktionen durchzuführen.
4. Implementierung einer sicheren
   Passwort-Speicherung (Hashing, Passwortregeln): Durch die Verwendung von Hashing-Algorithmen wie BCrypt stellen wir
   sicher, dass
   die Passwörter nicht im Klartext gespeichert werden und nicht leicht rückgängig gemacht werden können. Zusätzlich
   haben
   wir Passwortregeln festgelegt, um die Komplexität der Passwörter zu erhöhen. Diese Maßnahmen sind wichtig, um die
   Vertraulichkeit der Passwörter zu gewährleisten und die Auswirkungen von Datenlecks bei einem möglichen Angriff zu
   minimieren.
5. Strikte Inputvalidierung (für REST-Endpunkte und Datenbank): Indem wir Eingabedaten überprüfen und bereinigen,
   verhindern
   wir potenzielle Sicherheitslücken wie SQL-Injection und Cross-Site Scripting (XSS). Diese Validierung stellt sicher,
   dass die eingegebenen Daten den erwarteten Formaten entsprechen und keine bösartigen Inhalte enthalten. Dadurch
   verbessern wir die Sicherheit der Anwendung und schützen uns vor bekannten Angriffsmethoden.

Insgesamt bin ich mit meiner Arbeit zufrieden, da die grundlegenden Sicherheitsmechanismen erfolgreich implementiert
wurden.
Jedoch blieben einige Punkte noch offen, welche zusätzliche Sicherheit mitbringen würden.

6. Die Implementierung von CSRF-Protection beinhaltet die Generierung eines eindeutigen Tokens für jeden Benutzer und
   das Einbinden dieses Tokens in alle Formulare oder Anfragen, die schreibende Operationen durchführen. Beim Empfang
   einer Anfrage überprüft der Server, ob das CSRF-Token gültig und mit dem aktuellen Benutzer verknüpft ist. Wenn dies
   nicht der Fall ist, wird die Anfrage abgelehnt.

   Diese Implementierung funktioniert, weil sie den Angreifern das Fälschen von Anfragen erschwert. Das CSRF-Token ist
   ein
   geheimer Wert, der nur dem aktuellen Benutzer bekannt ist. Angreifer können dieses Token nicht abrufen oder
   generieren,
   da es nicht in ihrem Besitz ist. Daher können sie keine gültigen Anfragen im Namen des Benutzers senden, da sie das
   CSRF-Token nicht kennen. Dies stellt sicher, dass nur legitime Aktionen von autorisierten Benutzern ausgeführt werden
   können.
7. Security-relevante Unit-Tests können verschiedene Aspekte der Anwendung überprüfen, wie z.B. die korrekte Verarbeitung
von Benutzereingaben, die Validierung von Eingabedaten, die Verhinderung von SQL-Injection und Cross-Site Scripting (
XSS) sowie die Überprüfung von Zugriffsrechten und Berechtigungen.

   Durch regelmäßiges Ausführen dieser Tests während des Entwicklungsprozesses können potenzielle Sicherheitslücken
   frühzeitig erkannt und behoben werden. Dadurch wird die Wahrscheinlichkeit von Sicherheitsverletzungen und Datenlecks
   reduziert. Darüber hinaus tragen securityrelevante Tests dazu bei, das Sicherheitsbewusstsein des Entwicklungsteams zu
   stärken und bewährte Sicherheitspraktiken in den Entwicklungsprozess zu integrieren.