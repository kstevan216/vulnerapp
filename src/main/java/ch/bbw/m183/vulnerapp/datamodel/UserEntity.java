package ch.bbw.m183.vulnerapp.datamodel;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @NotBlank(message = "username cannot be blank")
    @Size(min = 3, max = 25
            , message = "only 3 to 25 characters are allowed, no sql statements 4 u!")
    private String username;

    @NotBlank(message = "username cannot be blank")
    @Pattern(regexp = "^[a-zA-Z ,.'-]+$"
            , message = "ur full name isn't supported")
    private String fullname;

    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*\\d).{8,}$"
            , message = "minimum of eight characters, at least one letter and one number")
    private String password;


}
