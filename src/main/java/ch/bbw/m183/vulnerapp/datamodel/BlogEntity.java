package ch.bbw.m183.vulnerapp.datamodel;

import java.time.LocalDateTime;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "blogs")
public class BlogEntity {

	@Id
	UUID id;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@Column(columnDefinition = "text")
	@NotBlank(message = "title cannot be blank")
	@Size(max = 50
			, message = "maximum of 50 characters")
	private String title;

	@Column(columnDefinition = "text")
	@NotBlank(message = "body cannot be blank")
	private String body;
}
