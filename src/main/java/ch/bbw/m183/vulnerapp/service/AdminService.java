package ch.bbw.m183.vulnerapp.service;

import java.util.stream.Stream;

import ch.bbw.m183.vulnerapp.datamodel.UserEntity;
import ch.bbw.m183.vulnerapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class AdminService {

    private final PasswordEncoder encoder;

    private final UserRepository repository;

    public UserEntity createUser(UserEntity newUser) {
        //Hash password
        newUser.setPassword(encoder.encode(newUser.getPassword()));
        return repository.save(newUser);
    }

    public Page<UserEntity> getUsers(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void deleteUser(String username) {
        repository.deleteById(username);
    }

    @EventListener(ContextRefreshedEvent.class)
    public void loadTestUsers() {
        Stream.of(new UserEntity().setUsername("admin").setFullname("Super Admin").setPassword("matchesRegex1"),
                        new UserEntity().setUsername("fuu").setFullname("Johanna Doe").setPassword("matchesRegex2"))
                .forEach(this::createUser);
    }
}
